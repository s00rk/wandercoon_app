window.HOST  = 'https://wandercoon.com/';
//window.HOST  = 'http://localhost:8000/';
document.addEventListener("deviceready", onDeviceReady, false);
var FILESYSTEM;
var layer;
var puntos;
var urls = [];
var	locals = [];
var markers = new L.FeatureGroup();
var mapa;
var routing;
var lastpos;
//localStorage.Online = 1;

Array.prototype.unique = function(a){
	return function()
	{
		return this.filter(a)
	}
}(function(a,b,c)
{
	return c.indexOf(a,b+1)<0
}
);



function onDeviceReady(){

	var networkState = navigator.connection.type;
	if(localStorage.token != '' && (networkState == Connection.UNKNOWN || networkState == Connection.CELL || networkState == Connection.WIFI || networkState == Connection.CELL_3G || networkState == Connection.CELL_4G))
	{
		localStorage.Online = 1;
		$.get(HOST + 'api/login/', { 'token': localStorage.token }, function(resp){
			console.log(resp.success);
			if(resp.success == 'false'){
				window.location = 'index.html';
			}
		});
	}

	var user = JSON.parse(localStorage.User);

	$('#nameUser').html(user.username);

	var token = 'pk.eyJ1IjoiczAwcmsiLCJhIjoiY2lodjdiNjAyMDF6c3Rma2g0OHhleGFpMCJ9.ONqocNCPpt0_Q6AmxtkTJw';
	var project = 'wandercoon';
	layer = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + token, {id: 's00rk.ofoha4pi'});

	var networkState = navigator.connection.type;
	if(localStorage.Online == 0)
	{
		//window.layer = L.tileLayer(cordova.file.dataDirectory + 'tiles/{z}/{x}/{y}.png');


		console.log('offline');
		console.log(localStorage.mapOffline + '/{z}/{x}/{y}.png');
		layer = L.tileLayer('cdvfile://localhost/persistent/tiles/{z}/{x}/{y}.png');
	}

	cargarMapa();

}

/*
$(document).on('ready', function(){
	var token = 'pk.eyJ1IjoiczAwcmsiLCJhIjoiY2lodjdiNjAyMDF6c3Rma2g0OHhleGFpMCJ9.ONqocNCPpt0_Q6AmxtkTJw';
	var project = 'wandercoon';
	layer = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + token, {id: 's00rk.ofoha4pi'});
	cargarMapa();
});
*/



function downloadFile(index){
	window.requestFileSystem(
		LocalFileSystem.PERSISTENT, 0,
		function(fileSystem)
		{
			var directoryEntry = fileSystem.root;
			var rootDir = fileSystem.root.fullPath;
			if (rootDir[rootDir.length-1] != '/') { rootDir += '/'; }
			var dirPath = rootDir;

			console.log(cordova.file.dataDirectory);
			var fileTransfer = new FileTransfer();
			var token = 'pk.eyJ1IjoiczAwcmsiLCJhIjoiY2lodjdiNjAyMDF6c3Rma2g0OHhleGFpMCJ9.ONqocNCPpt0_Q6AmxtkTJw';
			directoryEntry.getDirectory('tiles', { create: true, exclusive: false }, null, null);
			directoryEntry.getDirectory('tiles/16', { create: true, exclusive: false }, null, null);

			var a = locals[index].split('/')[2];
			directoryEntry.getDirectory('tiles/16/' + a, { create: true, exclusive: false }, function(entry){

				//localStorage.mapOffline = c.replace('16/', '');
				locals[index] = locals[index].replace('tiles/16/', '');

				entry.getFile(locals[index].split('/')[1], {create: false}, function(){
					if((index+1) < urls.length){
						downloadFile(index+1);
					}
				}, function(){
					fileTransfer.download(
						urls[index] + '?access_token=' + token,
						'cdvfile://localhost/persistent/tiles/16/' + locals[index],
						function(theFile) {
							console.log(theFile.toURL());
							if((index+1) < urls.length)
							{
								downloadFile(index+1);
							}
						},
						function(error) {
							if((index+1) < urls.length)
							{
								downloadFile(index+1);
							}
							console.log("download error source " + error.source);
							console.log("download error target " + error.target);
							console.log("download error code: " + error.code); }
							);
				})
			}, function(){console.log('no se creo dir');});



		},
		function() { alert("Failure!"); } //filesystem failure
		);
}

function whereiam (lat, longi, acc, mapa){
	var radius = acc / 2;
	if(window.whereiam)
	{
		mapa.removeLayer(window.whereiam);
	}
	window.whereiam = L.circle([lat, longi], radius).addTo(mapa);
}

function descargarKms(){

	var central = $('img:first')[0];
	central = central.src;


	var store = 'tiles/16/';
	var url = 'https://api.tiles.mapbox.com/v4/s00rk.ofoha4pi/16/';

	//central = 'https://api.tiles.mapbox.com/v4/s00rk.ofoha4pi/16/13204/28111.png?access_token=pk.eyJ1IjoiczAwcmsiLCJhIjoiY2lodjdiNjAyMDF6c3Rma2g0OHhleGFpMCJ9.ONqocNCPpt0_Q6AmxtkTJw';

	central = central.replace(url, '');
	central = central.substring(0, central.indexOf('.png'));
	var centro = central;
	central = central.split('/');
	var a = parseInt(central[0]);
	var b = parseInt(central[1]);

	urls = [];
	locals = [];
	var p = 0;

	//console.log(url + centro + '.png?access_token=pk.eyJ1IjoiczAwcmsiLCJhIjoiY2lodjdiNjAyMDF6c3Rma2g0OHhleGFpMCJ9.ONqocNCPpt0_Q6AmxtkTJw ' + store + centro);
	//downloadFile(url + centro, store + centro);
	urls[p] = url + centro + '.png';
	locals[p] = store + centro + '.png';
	p = p+1;

	//debe ser 14
	c = a+14;
	d = b+14;
	e = a-14;
	f = b-14;

	for(i = e; i <= c; i++){
		for(j = f; j <= d; j++){
			urls[p] = url + i + '/' + j + '.png';
			locals[p] = store + i + '/' + j + '.png';
			p = p +1;
		}
	}

	urls = urls.unique();
	locals = locals.unique();
	console.log(urls.length);
	console.log(p);
	downloadFile(0);
}

function cargarMapa(){
	var h = $( document ).height();
	var w = $( document ).width();
	$('#map').height(h-92);
	$('#map').width(w);

	mapa = L.map('map').setView([24.769882441181103, -107.46463537216185], 16).addLayer(layer);

	$('#whereiam').on('click', function(e){

		navigator.geolocation.getCurrentPosition(
			function(position){
				var coords = position.coords;
				mapa.setView([coords.latitude, coords.longitude], 16);
				whereiam(coords.latitude, coords.longitude, coords.accuracy, mapa);
			},
			function(error){}
			);

	});

	var menuLeft 	= document.getElementById( 'cbp-spmenu-s1' ),
	btnmenu		= document.getElementById( 'btnmenu' );

	btnmenu.onclick = function(){
		classie.toggle( this, 'active' );
		classie.toggle( menuLeft, 'cbp-spmenu-open' );
	};

	$('#cerrarsesion').on('click', function(){
		localStorage.token = '';
		localStorage.User = JSON.stringify({});
		window.location = 'index.html';
	});

	cargarRuta(mapa);
	navigator.geolocation.getCurrentPosition(
		function(position){
			var coords = position.coords;
			mapa.setView([coords.latitude, coords.longitude], 16);
			whereiam(coords.latitude, coords.longitude, coords.accuracy, mapa);
			localStorage.whereiam = JSON.stringify({'lat': coords.latitude, 'long': coords.longitude});
			if(localStorage.Online == 1){
				descargarKms();
			}

		},
		function(error){}
		);




}

function openModal(i){
	$('#modalEmpresa').modal('show');
	$('#modalEmpresa>div').css('margin-top', '120px');

	$('#descripcion').html( puntos[i].descripcion );
	$('#imagen').attr('src', puntos[i].logo );
	$('#telefono').html(puntos[i].telefonos );
	$('#direccion').html(puntos[i].direccion );
	$('#horario').html(puntos[i].hora_entrada + ' - ' + puntos[i].hora_salir );

}
var azulPin = L.icon({
	iconUrl: 'img/pin1.png',

	iconSize:     [29, 41],
	iconAnchor:   [12.5, 41],
	popupAnchor:  [-3, -76]
});
var naranjaPin = L.icon({
	iconUrl: 'img/pin2.png',

	iconSize:     [29, 41],
	iconAnchor:   [12.5, 41],
	popupAnchor:  [-3, -76]
});

var rojoPin = L.icon({
	iconUrl: 'img/pin3.png',

	iconSize:     [29, 41],
	iconAnchor:   [12.5, 41],
	popupAnchor:  [-3, -76]
});

var rosaPin = L.icon({
	iconUrl: 'img/pin4.png',

	iconSize:     [29, 41],
	iconAnchor:   [12.5, 41],
	popupAnchor:  [-3, -76]
});
var verdePin = L.icon({
	iconUrl: 'img/pin5.png',

	iconSize:     [29, 41],
	iconAnchor:   [12.5, 41],
	popupAnchor:  [-3, -76]
});
var amarilloPin = L.icon({
	iconUrl: 'img/pin6.png',

	iconSize:     [29, 41],
	iconAnchor:   [12.5, 41],
	popupAnchor:  [-3, -76]
});
var wanderPin = L.icon({
	iconUrl: 'img/pin7.png',

	iconSize:     [29, 41],
	iconAnchor:   [12.5, 41],
	popupAnchor:  [-3, -76]
});
function cargarLugares(){
	routing.spliceWaypoints(0, lastpos);
	puntos = [];
	var ways = [];
	$.each(JSON.parse(localStorage.Locales), function(i, data){
		ways[i] = L.latLng(data.lat, data.lon);
		puntos[i] = data;

		cate = data.categorias[0].nombre;
		if(cate == 'Cultural')
		{
			icono = azulPin;
		}else if(cate == 'Shopping'){
			icono = rojoPin;
		}else if(cate == 'Aventura'){
			icono == verdePin;
		}else if(cate == 'Culinaria'){
			icono = naranjaPin;
		}else if(cate == 'Fiesta'){
			icono = rosaPin;
		}else if (cate == 'Alojamiento'){
			icono == amarilloPin
		}else if (cate == 'Wanderden'){
			icono == wanderPin
		}
		var markerr = L.marker(ways[i], {icon: icono});
		markerr.on('click', window.Marcador);
		markers.addLayer(markerr);

	});
	mapa.addLayer(markers);
}

function cargarRuta(mapa){


	puntos = [];
	var ways = [];
	if(localStorage.Online == 0){
		cargarLugares();

		$.each(JSON.parse(localStorage.Rutas), function(i, data){
			$('#rutassystem').append('<a href="#" class="list-group-item" id="ruta' + i + '" onclick="rutaSystem(' + i + ');">' + data.nombre + '</a>');
		});

		return;
	}

	$.get(HOST + 'api/lugar/', function(resp){

		$.each(resp, function(i, data){
			ways[i] = L.latLng(data.lat, data.lon);
			puntos[i] = data;

			cate = data.categorias[0].nombre;
			if(cate == 'Cultural')
			{
				icono = verdePin;
			}else if(cate == 'Shopping'){
				icono = rosaPin;
			}else if(cate == 'Aventura'){
				icono == azulPin;
			}else if(cate == 'Culinaria'){
				icono = rojoPin;
			}else if(cate == 'Fiesta'){
				icono = naranjaPin;
			}else if (cate == 'Alojamiento'){
				icono == amarilloPin
			}else if (cate == 'Wanderden'){
				icono == wanderPin
			}
			var markerr = L.marker(ways[i], {icon: icono});
			markerr.on('click', window.Marcador);
			markers.addLayer(markerr);

		});
		mapa.addLayer(markers);
		localStorage.Locales = JSON.stringify(resp);


	});

	console.log(HOST + 'api/rutas/');
	$.get(HOST + 'api/rutas/', function(resp){

		$.each(resp, function(i, data){
			console.log();
			$('#rutassystem').append('<a href="#" class="list-group-item" onclick="rutaSystem(' + i + ');">' + data.nombre + '</a>');
		});

		localStorage.Rutas = JSON.stringify(resp);
	});



}

function rutaSystem(pos){

	if(localStorage.Rutas.length != 0)
	{
		mapa.removeLayer(markers);
		var rr = JSON.parse(localStorage.Rutas);
		lastpos = rr[pos]['lugares'].length;
		routing = L.Routing.control({
			waypoints: rr[pos]['lugares'],
			routeWhileDragging: false,
			router: L.Routing.graphHopper('6616f1de-a995-48b8-8cdb-a048a43d5c9a')
		}).addTo(mapa);
		$('.modal').modal('hide');
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
	}
};


window.Marcador = function(e){
	var lat = e.latlng.lat;
	var lng = e.latlng.lng;
	$.each(puntos, function(i, data){
		if(data.lat == lat && lng == data.lon){
			openModal(i);
		}
	});
};
