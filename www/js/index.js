window.HOST  = 'https://wandercoon.com/';
document.addEventListener("deviceready", onDeviceReady, false);
localStorage.Online = 0;

function onDeviceReady(){
	console.log(localStorage.token);
	/* Auto Login by Token */
	var networkState = navigator.connection.type;
	if(localStorage.token != '' && (networkState == Connection.UNKNOWN || networkState == Connection.CELL || networkState == Connection.WIFI || networkState == Connection.CELL_3G || networkState == Connection.CELL_4G))
	{
		localStorage.Online = 1;
		$.get(HOST + 'api/login/', { 'token': localStorage.token }, function(resp){
			console.log(resp.success);
			if(resp.success == 'true'){
				window.location = 'mapa.html';
			}
		});
	}else if(localStorage.token != ''){
		window.location = 'mapa.html';
	}
}

$(document).on('ready', function(){

    /* Login First Time */
	$('#frmLogin').on('submit', function(e){
		e.preventDefault();
		var usuario = $('#usuario').val();
		var contrasena = $('#contrasena').val();
		var err = false;

		if(usuario == '' || contrasena == ''){
			navigator.notification.alert('No se permiten campos vacios', null, 'Wandercoon', 'Aceptar');
			err = true;
		}

		if(err)
			return;

		$.post(HOST + 'api/login/', { 'usuario': usuario, 'contrasena': contrasena }, function(resp){
			if(resp.success == 'true')
			{
				localStorage.token = resp.token;
				localStorage.User = JSON.stringify(resp.user);
				window.location = 'mapa.html';
			}else{
				navigator.notification.alert(resp.reason, null, 'Wandercoon', 'Aceptar');
			}
			
		});

	});

	/* Register */
	$('#frmRegistro').on('submit', function(e){
		e.preventDefault();
		var usuario = $('#usuario').val();
		var contrasena = $('#contrasena').val();
		var email = $('#email').val();
		var telefono = $('#telefono').val();
		var re_contrasena = $('#re_contrasena').val();
		var err = false;

		if(contrasena != re_contrasena)
		{
			navigator.notification.alert('Las contraseñas no coinciden', null, 'Wandercoon', 'Aceptar');
			err = true;
		}else if(contrasena == ''){
			navigator.notification.alert('La contraseña no debe estar vacia', null, 'Wandercoon', 'Aceptar');
			err = true;
		}else if(usuario == '' || email == '' || telefono == ''){
			navigator.notification.alert('No se permiten campos vacios', null, 'Wandercoon', 'Aceptar');
			err = true;
		}

		if(err)
			return;

		$.post(HOST + 'api/registro/', { 'usuario': usuario, 'contrasena': contrasena, 'telefono': telefono, 'email': email, 're_contrasena': re_contrasena }, function(resp){
			if(resp.success == 'true')
			{
				localStorage.token = resp.token;
				localStorage.User = JSON.stringify(resp.user);
				window.location = 'mapa.html';
			}else{
				navigator.notification.alert(resp.reason, null, 'Wandercoon', 'Aceptar');
			}
			
		});

	});

	/* Restarurar contraseña */
	$('#btnRecuperar').on('click', function(e){
		e.preventDefault();

		var email = $('#rec_email').val();
		if(email == ''){
			navigator.notification.alert('No se permiten campos vacios', null, 'Wandercoon', 'Aceptar');
			return;
		}

		$.post(HOST + 'api/restaurar/', { 'email': email }, function(resp){
			if(resp.success == 'true'){
				navigator.notification.alert('Se te a enviado un correo, para restaurar tu contraseña', null, 'Wandercoon', 'Aceptar');
				$('#recuperar').modal('hide');
			}else{
				navigator.notification.alert(resp.reason, null, 'Wandercoon', 'Aceptar');
			}
		});
	});

});