window.HOST  = 'https://wandercoon.com/';
//window.HOST  = 'http://localhost:8000/'; 
document.addEventListener("deviceready", onDeviceReady, false);

/*
$(document).on('ready', function(){
	inicio();
});
*/

function onDeviceReady(){
	inicio();
}

function inicio() {

	var networkState = navigator.connection.type;
	if(localStorage.token != '' && (networkState == Connection.UNKNOWN || networkState == Connection.CELL || networkState == Connection.WIFI || networkState == Connection.CELL_3G || networkState == Connection.CELL_4G))
	{
		localStorage.Online = 1;
		$.get(HOST + 'api/login/', { 'token': localStorage.token }, function(resp){
			console.log(resp.success);
			if(resp.success == 'false'){
				window.location = 'index.html';
			}
		});
	}

	var user = JSON.parse(localStorage.User);

	$('#nameUser').html(user.username);
	console.log(localStorage.User);

	var menuLeft 	= document.getElementById( 'cbp-spmenu-s1' ),
	btnmenu		= document.getElementById( 'btnmenu' );

	btnmenu.onclick = function(){
		classie.toggle( this, 'active' );
		classie.toggle( menuLeft, 'cbp-spmenu-open' );
	};

	$('.btn-file :file').on('fileselect', function(event, numFiles, label) {

		var input = $(this).parents('.input-group').find(':text'),
		log = numFiles > 1 ? numFiles + ' files selected' : label;

		if( input.length ) {
			input.val(log);
		}

	});

	$('#cerrarsesion').on('click', function(){
		localStorage.token = '';
		localStorage.User = JSON.stringify({});
		window.location = 'index.html';
	});

	if(user.foto.length != 0)
	{
		$('#frmLogin img').attr('src', user.foto);	
	}
	$('#usuario').val(user.username);
	$('#telefono').val(user.telefono);
	$('#email').val(user.email);

	$('#frmLogin button').on('click', function(e){
		e.preventDefault();
		var formData = new FormData($('#frmLogin')[0]);
		$.ajax({
			url: window.HOST + 'api/login/',
			type: 'POST',
			headers: {
				'apitoken': localStorage.token
			},
			success: function(resp){
				localStorage.User = JSON.stringify(resp.user);
				navigator.notification.alert(resp.reason, null, 'Wandercoon', 'Aceptar');
				window.location = 'perfil.html';
			},
			data: formData,

			cache: false,
			contentType: false,
			processData: false
		});
	});
}

$(document).on('change', '.btn-file :file', function() {
	var input = $(this),
	numFiles = input.get(0).files ? input.get(0).files.length : 1,
	label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [numFiles, label]);
});